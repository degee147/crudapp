<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Services Controller
 *
 */
class ServicesController extends AppController
{
    public function beforeFilter(\Cake\Event\Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow();
    }
    public function index(){
        
    }
}

