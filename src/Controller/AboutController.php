<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * About Controller
 *
 */
class AboutController extends AppController
{
    public function beforeFilter(\Cake\Event\Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow();
    }
    public function index(){
        
    }
}
